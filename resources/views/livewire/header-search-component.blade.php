<div class="wrap-search center-section">
    <div class="wrap-search-form">
        <form action="{{ route('busqueda.producto') }}" id="form-search-top" name="form-search-top">
            <input type="text" name="search" value="{{ $busqueda }}" placeholder="Ingresa tu busqueda">
            <button form="form-search-top" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            <div class="wrap-list-cate">
                <input type="hidden" name="producto_cat" value="{{ $producto_cat }}" id="producto-cat">
                <input type="hidden" name="producto_cat_id" value="{{ $producto_cat_id }}" id="producto-cat-id">
                <a href="#" class="link-control">{{ str_split($producto_cat,12)[0] }}</a>
                <ul class="list-cate">
                    <li class="level-0">Categorias</li>
                    @foreach ($categorias as $categoria)
                        <li class="level-1" data-id="{{ $categoria->id }}">{{ $categoria->nombre }}</li>
                    @endforeach
                </ul>
            </div>
        </form>
    </div>
</div>