<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">home</a></li>
                <li class="item-link"><span>Checkout</span></li>
            </ul>
        </div>
        <div class=" main-content-area">
            <form wire:submit.prevent="placeOrder">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrap-address-billing">
                            <h3 class="box-title">Dirección de Envio</h3>
                            <div class="billing-address">
                                <p class="row-in-form">
                                    <label for="fname">Nombre<span>*</span></label>
                                    <input type="text" name="fname" value="" placeholder="Su Nombre" wire:model="nombre">
                                    @error('nombre')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="lname">Apellidos<span>*</span></label>
                                    <input type="text" name="lname" value="" placeholder="Sus Apellidos" wire:model="apellido">
                                    @error('apellido')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" value="" placeholder="Correo Electronico" wire:model="email">
                                    @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="phone">Número Telefonico<span>*</span></label>
                                    <input type="number" name="phone" value="" placeholder="Con un formato de 10 digitos" wire:model="telefono">
                                    @error('telefono')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="add">Dirección 1:</label>
                                    <input type="text" name="add" value="" placeholder="Calle o el número de apartamento" wire:model="line1">
                                    @error('line1')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="add">Dirección 2:</label>
                                    <input type="text" name="add" value="" placeholder="Calle o el número de apartamento" wire:model="line2">
                                </p>
                                <p class="row-in-form">
                                    <label for="country">País<span>*</span></label>
                                    <input type="text" name="country" value="" placeholder="Mexico" wire:model="pais">
                                    @error('pais')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="provincia">Provincia<span>*</span></label>
                                    <input type="text" name="provincia" value="" placeholder="Provincia" wire:model="provincia">
                                    @error('provincia')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="city">Pueblo o Ciudad<span>*</span></label>
                                    <input type="text" name="city" value="" placeholder="Nombre de la Ciudad" wire:model="ciudad">
                                    @error('ciudad')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="zip-code">Codigo Postal</label>
                                    <input type="number" name="zip-code" value="" placeholder="Su Codigo Postal" wire:model="codigopostal">
                                    @error('codigopostal')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form fill-wife">
                                    <label class="checkbox-field">
                                        <input name="different-add" id="different-add" value="1" type="checkbox" wire:model="ship_to_different">
                                        <span>¿Envia a una direccion diferente?</span>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                    @if ($ship_to_different)
                    <div class="col-md-12">
                        <div class="wrap-address-billing">
                            <h3 class="box-title">Dirección de Envio Propia</h3>
                            <div class="billing-address">
                                <p class="row-in-form">
                                    <label for="fname">Nombre<span>*</span></label>
                                    <input type="text" name="fname" value="" placeholder="Su Nombre" wire:model="s_nombre">
                                    @error('s_nombre')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="lname">Apellidos<span>*</span></label>
                                    <input type="text" name="lname" value="" placeholder="Sus Apellidos" wire:model="s_apellido">
                                    @error('s_apellido')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" value="" placeholder="Correo Electronico" wire:model="s_email">
                                    @error('s_email')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="phone">Número Telefonico<span>*</span></label>
                                    <input type="number" name="phone" value="" placeholder="Con un formato de 10 digitos" wire:model="s_telefono">
                                    @error('s_telefono')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="add">Dirección 1:</label>
                                    <input type="text" name="add" value="" placeholder="Calle o el número de apartamento" wire:model="s_line1">
                                    @error('s_line1')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="add">Dirección 2:</label>
                                    <input type="text" name="add" value="" placeholder="Calle o el número de apartamento" wire:model="s_line2">
                                </p>
                                <p class="row-in-form">
                                    <label for="country">País<span>*</span></label>
                                    <input type="text" name="country" value="" placeholder="Mexico" wire:model="s_pais">
                                    @error('s_pais')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="provincia">Provincia<span>*</span></label>
                                    <input type="text" name="provincia" value="" placeholder="Provincia" wire:model="s_provincia">
                                    @error('s_provincia')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="city">Pueblo o Ciudad<span>*</span></label>
                                    <input type="text" name="city" value="" placeholder="Nombre de la Ciudad" wire:model="s_ciudad">
                                    @error('s_ciudad')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                                <p class="row-in-form">
                                    <label for="zip-code">Codigo Postal</label>
                                    <input type="number" name="zip-code" value="" placeholder="Su Codigo Postal" wire:model="s_codigopostal">
                                    @error('s_codigopostal')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                
                <div class="summary summary-checkout">
                    <div class="summary-item payment-method">
                        <h4 class="title-box">Método de pago</h4>
                        <p class="summary-info"><span class="title">Cheque / Efectivo</span></p>
                        <p class="summary-info"><span class="title">Tarjeta de crédito (guardada)</span></p>
                        <div class="choose-payment-methods">
                            <label class="payment-method">
                                <input name="payment-method" id="payment-method-bank" value="cod" type="radio" wire:model="metodoPago">
                                <span>Pago Contra Reembolso <b>(COD)</b></span>
                                <span class="payment-desc">Ordena ahora, pague en la entrega</span>
                            </label>
                            <fieldset disabled>
                                <label class="payment-method">
                                    <input name="payment-method" id="payment-method-visa" value="card" type="radio" wire:model="metodoPago">
                                    <span>Tarjeta de Credito o Debito</span>
                                    <span class="payment-desc">Hay muchas variaciones de pasajes de Lorem Ipsum disponibles.</span>
                                </label>
                                <label class="payment-method">
                                    <input name="payment-method" id="payment-method-paypal" value="paypal" type="radio" wire:model="metodoPago">
                                    <span>Paypal</span>
                                    <span class="payment-desc">Puedes pagar con tu tarjeta de</span>
                                    <span class="payment-desc">credito si no tienes una cuenta paypal</span>
                                </label>
                            </fieldset>
                            @error('metodoPago')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        @if (Session::has('checkout'))
                            <p class="summary-info grand-total"><span>Total</span> <span class="grand-total-price">${{ Session::get('checkout')['total'] }}</span></p>
                        @endif
                        <button type="submit" class="btn btn-medium">Realizar Orden</button>
                    </div>
                    <div class="summary-item shipping-method">
                        <h4 class="title-box f-title">Método de envío</h4>
                        <p class="summary-info"><span class="title">Tarifa plana</span></p>
                        <p class="summary-info"><span class="title">Valor Fijo $0</span></p>
                    </div>
                </div>
            </form>

        </div><!--end main content area-->
    </div><!--end container-->

</main>