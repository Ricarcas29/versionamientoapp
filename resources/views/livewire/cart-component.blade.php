<main id="main" class="main-site">

    <div class="container">

        <div class="wrap-breadcrumb">
            <ul>
                <li class="item-link"><a href="/" class="link">home</a></li>
                <li class="item-link"><span>Carrito</span></li>
            </ul>
        </div>
        <div class=" main-content-area">
            @if (Cart::instance('cart')->count() > 0)
                <div class="wrap-iten-in-cart">
                    @if (Session::has('success_message'))
                        <div class="alert alert-success">
                            <strong>!Listo¡ </strong>{{ Session::get('success_message') }}
                        </div>
                    @endif
                    @if (Cart::instance('cart')->count() > 0)
                    <h3 class="box-title">Productos seleccionados</h3>
                    <ul class="products-cart">
                        @foreach (Cart::instance('cart')->content() as $item)
                            <li class="pr-cart-item">
                                <div class="product-image">
                                    <figure><img src="{{ asset('assets/images/products') }}/{{ $item->model->imagen }}" alt="{{ $item->model->nombre }}"></figure>
                                </div>
                                <div class="product-name">
                                    <a class="link-to-product" href="{{ route('producto.detalles',['slug'=>$item->model->slug]) }}">{{ $item->model->nombre }}</a>
                                </div>
                                @if ($item->model->precio_venta > 0)
                                    <div class="price-field produtc-price">
                                        <p class="price">${{ $item->model->precio_venta }}</p>
                                    </div>
                                @else
                                    <div class="price-field produtc-price">
                                        <p class="price">${{ $item->model->precio_regular }}</p>
                                    </div>
                                @endif
                                <div class="quantity">
                                    <div class="quantity-input">
                                        <input type="text" name="product-quatity" value="{{ $item->qty }}" data-max="120" pattern="[0-9]*" >									
                                        <a class="btn btn-increase" href="#" wire:click.prevent="incrementa('{{ $item->rowId }}')"></a>
                                        <a class="btn btn-reduce" href="#" wire:click.prevent="decrementa('{{ $item->rowId }}')"></a>
                                    </div>
                                </div>
                                <div class="price-field sub-total"><p class="price">${{ $item->subtotal }}</p></div>
                                <div class="delete">
                                    <a href="#" wire:click.prevent="destroy('{{ $item->rowId }}')" class="btn btn-delete" title="">
                                        <span>Delete from your cart</span>
                                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </li>	
                        @endforeach							
                    </ul>  
                    @else
                        <p>No hay productos agregados al Carrito</p>
                    @endif
                </div>

                <div class="summary">
                    <div class="order-summary">
                        <h4 class="title-box">Order Summary</h4>
                        <p class="summary-info"><span class="title">Subtotal</span><b class="index">${{ number_format(Cart::instance('cart')->subtotal(),2) }}</b></p>
                        @if (Session::has('cupon'))
                            <p class="summary-info"><span class="title">Descuento ({{ Session::get('cupon')['codigo'] }}) <a href="#" wire:click.prevent="quitarCupon"><i class="fa fa-times text-danger"></i></a></span><b class="index text-danger"> -${{ number_format($descuento,2) }}</b></p>
                            <p class="summary-info"><span class="title">Subtutal con Descuento</span><b class="index">${{ number_format($subtotalDespuesDescuento,2) }}</b></p>
                            <p class="summary-info"><span class="title">IVA <b>({{ config('cart.tax') }}%)</b></span><b class="index">${{ number_format($ivaDespuesDescuento,2) }}</b></p>
                            <p class="summary-info total-info "><span class="title">Total</span><b class="index">${{ number_format($totalDespuesDescuento,2) }}</b></p>
                        @else    
                            <p class="summary-info"><span class="title">IVA <b>({{ config('cart.tax') }}%)</b></span><b class="index">${{ number_format(Cart::instance('cart')->tax(),2) }}</b></p>
                            <p class="summary-info"><span class="title">Shipping</span><b class="index">Free Shipping</b></p>
                            <p class="summary-info total-info "><span class="title">Total</span><b class="index">${{ number_format(Cart::instance('cart')->total(),2) }}</b></p>
                        @endif
                    </div>
                    <div class="checkout-info">
                        @if (!Session::has('cupon'))
                            <label class="checkbox-field">
                                <input class="frm-input " name="have-code" id="have-code" value="1" type="checkbox" wire:model="tengoCodigoCupon"><span>Tengo un cupón de descuento</span>
                            </label>
                            @if ($tengoCodigoCupon == 1)
                                <div class="summary-item">
                                    <form wire:submit.prevent="aplicarCodigoCupon">
                                        <h4 class="title-box">Codigo del Cupón</h4>
                                        @if (Session::has('cupon_message'))
                                            <div class="alert alert-danger" role="danger"><strong>!Ups¡ </strong>{{ Session::get('cupon_message') }}</div>
                                        @endif
                                        <p class="row-in-form">
                                            <label for="codigo-cupon">Ingresa el codigo del cupón:</label>
                                            <input type="text" name="codigo-cupon" wire:model="codigoCupon" />
                                        </p>
                                        <button type="submit" class="btn btn-small">Aplicar</button>
                                    </form>
                                </div>
                            @endif
                        @endif
                        <a class="btn btn-checkout" href="#" wire:click.prevent="checkout">Proceder a pagar</a>
                        <a class="link-to-shop" href="/shop">Continuar comprando<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="update-clear">
                        <a class="btn btn-clear" href="#" wire:click.prevent="destroyAll()">Limpiar carrtito de compras</a>
                        <a class="btn btn-update" href="/cart">Actualizar carrito de compras</a>
                    </div>
                </div>
            @else
                <div class="text-center">
                    <h1>Tu carrito de compras está vacío</h1>
                    <p>Seguro encuentras algo interesante !!!</p>
                    <a href="/shop" class="btn btn-success">Ver Tienda</a>
                </div>
            @endif
            {{-- 
            <div class="wrap-show-advance-info-box style-1 box-in-site">
                <h3 class="title-box">Most Viewed Products</h3>
                <div class="wrap-products">
                    <div class="products slide-carousel owl-carousel style-nav-1 equal-container" data-items="5" data-loop="false" data-nav="true" data-dots="false" data-responsive='{"0":{"items":"1"},"480":{"items":"2"},"768":{"items":"3"},"992":{"items":"3"},"1200":{"items":"5"}}' >

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_4.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">new</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_17.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item sale-label">sale</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_15.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">new</span>
                                    <span class="flash-item sale-label">sale</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_1.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item bestseller-label">Bestseller</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_21.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_3.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item sale-label">sale</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><ins><p class="product-price">$168.00</p></ins> <del><p class="product-price">$250.00</p></del></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_4.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item new-label">new</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>

                        <div class="product product-style-2 equal-elem ">
                            <div class="product-thumnail">
                                <a href="#" title="T-Shirt Raw Hem Organic Boro Constrast Denim">
                                    <figure><img src="{{ asset('assets/images/products/digital_5.jpg') }}" width="214" height="214" alt="T-Shirt Raw Hem Organic Boro Constrast Denim"></figure>
                                </a>
                                <div class="group-flash">
                                    <span class="flash-item bestseller-label">Bestseller</span>
                                </div>
                                <div class="wrap-btn">
                                    <a href="#" class="function-link">quick view</a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="#" class="product-name"><span>Radiant-360 R6 Wireless Omnidirectional Speaker [White]</span></a>
                                <div class="wrap-price"><span class="product-price">$250.00</span></div>
                            </div>
                        </div>
                    </div>
                </div><!--End wrap-products-->
            </div>
             --}}
        </div><!--end main content area-->
    </div><!--end container-->

</main>