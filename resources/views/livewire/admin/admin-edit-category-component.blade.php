<div>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Editar Categoria</h5> 
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.categorias') }}" class="btn btn-success pull-right">Todas las Categorias</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal container-form" wire:submit.prevent="actualizarCategoria">
                            <div class="mb-3 from-group">
                                <label class="form-label">Nombre de la Categoria</label>
                                <input type="text" placeholder="Nombre de la Categoria" class="form-control" wire:model="nombre" wire:keyup="generarslug" required>
                                @error('nombre')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3 from-group mt">
                                <label class="form-label">Slug</label>
                                <input type="text" placeholder="Categoria Slug" class="form-control" wire:model="slug" required>
                                @error('slug')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mt">Actualizar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .container-form{
        margin: 40px !important;
        padding: 0px 100px !important;
    }

    .mt{
        margin-top: 20px !important;
    }
</style>