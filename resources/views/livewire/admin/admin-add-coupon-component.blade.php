<div>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Nuevo Cupón</h5> 
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.cupones') }}" class="btn btn-success pull-right">Todos los Cupones</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal container-form" wire:submit.prevent="guardarCupon">
                            <div class="mb-3 from-group">
                                <label class="form-label">Código del Cupón</label>
                                <input type="text" placeholder="Código del Cupón" class="form-control" wire:model="codigo" required>
                                @error('codigo')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3 from-group mt">
                                <label class="form-label">Tipo de Cupón</label>
                                <select class="form-select form-select-lg mb-3" aria-label="Default select example" wire:model="tipo" required>
                                    <option selected>Tipo</option>
                                    <option value="fixed">Fixed</option>
                                    <option value="percent">Porcentual</option>
                                </select>
                                @error('tipo')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3 from-group mt">
                                <label class="form-label">Valor del Cupón</label>
                                <input type="text" placeholder="Valor del Cupón" class="form-control" wire:model="valor" required>
                                @error('valor')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="mb-3 from-group">
                                <label class="form-label">Valor del carrito</label>
                                <input type="text" placeholder="Valor del carrito" class="form-control" wire:model="cart_valor" required>
                                @error('cart_valor')
                                    <p class="text-danger">{{ $message }}</p>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mt">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .container-form{
        margin: 40px !important;
        padding: 0px 100px !important;
    }

    .mt{
        margin-top: 20px !important;
    }
</style>