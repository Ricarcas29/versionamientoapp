<div>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Nuevo Slider</h5> 
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.homeslider') }}" class="btn btn-success pull-right">Todos los Sliders</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <form class="form-horizontal container-form" {{-- enctype="multipart/form-data" --}} wire:submit.prevent="guardarSlider">
                            <div class="mb-3 from-group">
                                <label class="form-label">Titulo</label>
                                <input type="text" placeholder="Titulo del Slider" class="form-control" wire:model="titulo">
                            </div>
                            <div class="mb-3 from-group">
                                <label class="form-label mt">Subtitulo</label>
                                <input type="text" placeholder="Subtitulo del Slider" class="form-control" wire:model="subtitulo">
                            </div>
                            <div class="mb-3 from-group">
                                <label class="form-label mt">Precio</label>
                                <input type="text" placeholder="Precio" class="form-control" wire:model="precio">
                            </div>
                            <div class="mb-3 from-group">
                                <label class="form-label mt">Link</label>
                                <input type="text" placeholder="Link" class="form-control" wire:model="link">
                            </div>
                            <div class="mb-3 from-group mt">
                                <label class="form-label mt">Imagen</label>
                                <input type="file"class="form-control" wire:model="imagen">
                                @if ($imagen)
                                    <img src="{{ $imagen->temporaryUrl() }}" width="120px">
                                @endif
                            </div>
                            <div class="mb-3 from-group mt">
                                <label class="form-label mt">Estado</label>
                                <select class="form-select form-select-lg mb-3" aria-label="Default select example" wire:model="estado">
                                    <option selected>Seleccione el estado</option>
                                    {{-- @foreach ($categorias as $categoria) --}}
                                        <option value="0">Inactivo</option>
                                        <option value="1">Activo</option>
                                    {{-- @endforeach  --}}
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary mt">Guardar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .container-form{
        margin: 40px !important;
        padding: 0px 100px !important;
    }

    .mt{
        margin-top: 20px !important;
    }
</style>