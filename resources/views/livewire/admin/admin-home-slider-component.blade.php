<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
    </style>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Todos los Sliders</h5>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addhomeslider') }}" class="btn btn-success pull-right">Nuevo Slider</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Imagen</th>
                                    <th>Titulo</th>
                                    <th>Subtitulo</th>
                                    <th>Precio</th>
                                    <th>Link</th>
                                    <th>Estatus</th>
                                    <th>Fecha</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sliders as $slider)
                                    <tr>
                                        <td>{{ $slider->id }}</td>
                                        <td><img src="{{ asset('assets/images/sliders') }}/{{ $slider->imagen }}" alt="{{ $slider->nombre }}" width="120px"></td>
                                        <td>{{ $slider->titulo }}</td>
                                        <td>{{ $slider->subtitulo }}</td>
                                        <td>{{ $slider->precio }}</td>
                                        <td>{{ $slider->link }}</td>
                                        <td>{{ $slider->estado == 1 ? 'Activo':'Inactivo' }}</td>
                                        <td>{{ $slider->created_at }}</td>
                                        <td>
                                            <a href="{{ route('admin.edithomeslider',['slider_id'=>$slider->id]) }}">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fa fa-edit fa-1x"></i>
                                                    Editar
                                                </button>
                                            </a>
                                            <a href="#" onclick="confirm('¿Estas seguro de querer eliminar esta producto?') || event.stopImmediatePropagation()" wire:click.prevent="eliminarSlider({{ $slider->id }})" style="margin-left: 15px;">
                                                <button type="button" class="btn btn-danger">
                                                    <i class="fa fa-times fa-1x"></i>
                                                    Eliminar
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- {{ $sliders->links() }} --}}
            </div>
        </div>
    </div>
</div>
