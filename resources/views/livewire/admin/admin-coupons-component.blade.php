<div>
    <style>
        nav svg{
            height: 20px;
        }
        nav .hidden{
            display: block !important;
        }
    </style>
    <div class="container" style="padding: 30px 0px;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 text-uppercase">
                                <h5>Todos los Cupones</h5>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('admin.addcupon') }}" class="btn btn-success pull-right">Nuevo Cupón</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Codigo del cupón</th>
                                    <th>Tipo de cupón</th>
                                    <th>Valor del cupón</th>
                                    <th>Valor del carrito</th>
                                    <th>Fecha de creación</th>
                                    <th {{-- style="width:300px!important;" --}}>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cupones as $cupon)
                                    <tr>
                                        <td>{{ $cupon->id }}</td>
                                        <td>{{ $cupon->codigo }}</td>
                                        <td>{{ $cupon->tipo }}</td>
                                        @if ($cupon->tipo == 'fixed')
                                            <td>{{ $cupon->valor }}</td>
                                        @else
                                            <td>{{ $cupon->valor }} %</td>
                                        @endif
                                        <td>{{ $cupon->cart_valor }}</td>
                                        <td>{{ $cupon->expery_date }}</td>
                                        <td>
                                            <a href="{{ route('admin.editcupon',['cupon_id'=>$cupon->id]) }}">
                                                <button type="button" class="btn btn-success">
                                                    <i class="fa fa-edit fa-1x"></i>
                                                    Editar
                                                </button>
                                            </a>
                                            <a href="#" onclick="confirm('¿Estas seguro de querer eliminar este cupón?') || event.stopImmediatePropagation()" wire:click.prevent="eliminarCupon({{ $cupon->id }})" style="margin-left: 15px;">
                                                <button type="button" class="btn btn-danger">
                                                    <i class="fa fa-times fa-1x"></i>
                                                    Eliminar
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
