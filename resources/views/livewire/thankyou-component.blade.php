<div>
    <main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="/" class="link">home</a></li>
					<li class="item-link"><span>Gracias</span></li>
				</ul>
			</div>
		</div>
		
		<div class="container pb-60">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2>Gracias por tu compra</h2>
                    <p>Tu orden ha sido enviada</p>
                    <a href="/shop" class="btn btn-submit btn-submitx">Continuar comprando</a>
				</div>
			</div>
		</div><

	</main>
</div>
