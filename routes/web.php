<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\ShopComponent;
use App\Http\Livewire\CartComponent;
use App\Http\Livewire\CheckoutComponent;
use App\Http\Livewire\DetailsComponent;
use App\Http\Livewire\CategoryComponent;
use App\Http\Livewire\SearchComponent;
use App\Http\Livewire\ThankyouComponent;
use App\Http\Livewire\AboutUsComponent;
use App\Http\Livewire\User\UserDashboardComponent;
use App\Http\Livewire\Admin\AdminDashboardComponent;
use App\Http\Livewire\Admin\AdminCategoryComponent;
use App\Http\Livewire\Admin\AdminAddCategoryComponent;
use App\Http\Livewire\Admin\AdminEditCategoryComponent;
use App\Http\Livewire\Admin\AdminProductComponent;
use App\Http\Livewire\Admin\AdminAddProductComponent;
use App\Http\Livewire\Admin\AdminEditProductComponent;
use App\Http\Livewire\Admin\AdminHomeSliderComponent;
use App\Http\Livewire\Admin\AdminAddHomeSliderComponent;
use App\Http\Livewire\Admin\AdminEditHomeSliderComponent;
use App\Http\Livewire\Admin\AdminHomeCategoryComponent;
use App\Http\Livewire\Admin\AdminCouponsComponent;
use App\Http\Livewire\Admin\AdminAddCouponComponent;
use App\Http\Livewire\Admin\AdminEditCouponComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', HomeComponent::class);

Route::get('/shop', ShopComponent::class);

Route::get('/cart', CartComponent::class)->name('producto.cart');

Route::get('/checkout', CheckoutComponent::class)->name('checkout');

Route::get('/producto/{slug}', DetailsComponent::class)->name('producto.detalles');

Route::get('/categoria-producto/{categoria_slug}', CategoryComponent::class)->name('categoria.producto');

Route::get('/busqueda', SearchComponent::class)->name('busqueda.producto');

Route::get('/gracias', ThankyouComponent::class)->name('gracias');

Route::get('/nosotros', AboutUsComponent::class)->name('nosotros');

//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//    return view('dashboard');
//})->name('dashboard');

//Ruta para el Cliente
Route::middleware(['auth:sanctum', 'verified'])->group(function(){
    Route::get('/user/dashboard', UserDashboardComponent::class)->name('user.dashboard');
});

//Ruta para el Administrador
Route::middleware(['auth:sanctum', 'verified', 'authadmin'])->group(function(){
    Route::get('/admin/dashboard', AdminDashboardComponent::class)->name('admin.dashboard');
    #Categorias
    Route::get('/admin/categorias', AdminCategoryComponent::class)->name('admin.categorias');
    Route::get('/admin/categoria/add', AdminAddCategoryComponent::class)->name('admin.addcategoria');
    Route::get('/admin/categoria/edit/{categoria_slug}', AdminEditCategoryComponent::class)->name('admin.editcategoria');
    #Productos
    Route::get('/admin/productos', AdminProductComponent::class)->name('admin.productos');
    Route::get('/admin/producto/add', AdminAddProductComponent::class)->name('admin.addproducto');
    Route::get('/admin/producto/edit/{producto_slug}', AdminEditProductComponent::class)->name('admin.editproducto');
    #Administración del Home
    Route::get('/admin/slider', AdminHomeSliderComponent::class)->name('admin.homeslider');
    Route::get('/admin/slider/add', AdminAddHomeSliderComponent::class)->name('admin.addhomeslider');
    Route::get('/admin/slider/edit/{slider_id}', AdminEditHomeSliderComponent::class)->name('admin.edithomeslider');
    Route::get('/admin/categorias-home', AdminHomeCategoryComponent::class)->name('admin.categoriashome');
    #Cupones
    Route::get('/admin/cupones', AdminCouponsComponent::class)->name('admin.cupones');
    Route::get('/admin/cupones/add', AdminAddCouponComponent::class)->name('admin.addcupon');
    Route::get('/admin/cupones/edit/{cupon_id}', AdminEditCouponComponent::class)->name('admin.editcupon');
});