<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Categoria;
use Livewire\WithPagination;

class AdminCategoryComponent extends Component
{
    use WithPagination;

    //Eliminar Categoria
    public function eliminarCategoria($id)
    {
        $categoria = Categoria::find($id);
        $categoria->delete();
        session()->flash('message','La categoria se ha eliminado exitosamente');
    }

    public function render()
    {
        $categorias = Categoria::paginate(5);
        return view('livewire.admin.admin-category-component',['categorias'=>$categorias])->layout('layouts.base');
    }
}
