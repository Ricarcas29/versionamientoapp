<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Categoria;
use Illuminate\Support\Str;

class AdminAddCategoryComponent extends Component
{
    public $nombre;
    public $slug;

    //Aunto completa el slug
    public function generarslug()
    {
        $this->slug = Str::slug($this->nombre);
    }

    public function updated($fields)
    {
        $this->validateOnly($fields,[
            'nombre' => 'required',
            'slug' => 'required|unique:categorias'
        ]);
    }

    //Guarda Categoria
    public function guardarCategoria()
    {
        $this->validate([
            'nombre' => 'required',
            'slug' => 'required|unique:categorias'
        ]);
        $categoria = new Categoria();
        $categoria->nombre = $this->nombre;
        $categoria->slug = $this->slug;
        $categoria->save();
        session()->flash('message','La categoria se ha creado exitosamente');
    }

    public function render()
    {
        return view('livewire.admin.admin-add-category-component')->layout('layouts.base');
    }
}
